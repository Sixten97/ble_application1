package com.example.ble_application02;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private BluetoothLeAdvertiser mBLE_advertiser;
    private AdvertiseSettings mAdvertiserSettings;
    private AdvertiseData mBleAdvertiseData;
    private BluetoothAdapter mBleAdapter;
    private BLE_5_system mBle5;

    private Button onButton,offButton;
    public TextView device_status, device_data, device_settings, device_mode;

    private ParcelUuid pUuid;

    private static final String BLE_UUID = "CDB7950D-73F1-4D4D-8E47-C090502DBD63";
    private boolean isBle5Compatible = false;

    public MainActivity() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBleAdapter = BluetoothAdapter.getDefaultAdapter();
        if( !mBleAdapter.isMultipleAdvertisementSupported() ) {
            Toast.makeText( this, "Multiple advertisement not supported", Toast.LENGTH_SHORT ).show();
            //mAdvertiseButton.setEnabled( false );
            //mDiscoverButton.setEnabled( false );
        }
        findViewByIdes();       //Initiates the gui
        //Runs if we can use BLE 5
        if(checkBLE5Compatibility()){
            mBle5 = new BLE_5_system(mBleAdapter, this);
            isBle5Compatible = true;
            mBle5.ble5Advertise();
            device_mode.setText("Mode: BLE 5");
        }
        //Runs if we can't use BLE 5
        else{
            try {
                advertise();
                device_mode.setText("Mode: BLE");
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "Advertising failed", Toast.LENGTH_SHORT).show();
                device_mode.setText("Mode: Fail");
            }
        }
        implementListeners();   //Sets up the listeners for the gui
    }

    //Sets up the listeners for the gui
    private void implementListeners() {
        onButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isBle5Compatible){
                    mBle5.ble5start();
                }
                else{
                    mBLE_advertiser.startAdvertising(mAdvertiserSettings, mBleAdvertiseData, mBleAdvCallback);
                }
            }
        });

        offButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isBle5Compatible){
                    mBle5.ble5stop();
                }
                else{
                    mBLE_advertiser.stopAdvertising(mBleAdvCallback);
                }
                device_status.setText("Status");
                device_data.setText("Data");
                device_settings.setText("Settings");
            }
        });
    }

    //Initiates the gui
    private void findViewByIdes() {
        onButton = (Button) findViewById(R.id.onButton);
        offButton = (Button) findViewById(R.id.offButton);
        device_status = (TextView) findViewById(R.id.device_Status);
        device_data = (TextView) findViewById(R.id.device_Data);
        device_settings = (TextView) findViewById(R.id.device_Settings);
        device_mode = (TextView) findViewById(R.id.device_Mode);

    }

    //Method for checking if we can use BLE 5
    private boolean checkBLE5Compatibility(){
        //Requires API level 26
        if(mBleAdapter.isLe2MPhySupported() && mBleAdapter.isLeCodedPhySupported() && mBleAdapter.isLeExtendedAdvertisingSupported() && mBleAdapter.isLePeriodicAdvertisingSupported()){
            return true;
        }
        else{
            Toast.makeText( this, "BLE 5 is a no go", Toast.LENGTH_SHORT ).show();
            //device_status.setText("No BLE 5");
            return false;
        }
    }

    //Sets up the advertising
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void advertise() throws UnsupportedEncodingException {
        mBLE_advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser(); //Requires API level 21 or higher
        mAdvertiserSettings = new AdvertiseSettings.Builder()
                .setAdvertiseMode( AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY )
                .setTxPowerLevel( AdvertiseSettings.ADVERTISE_TX_POWER_HIGH )
                .setConnectable( false )
                .build();
        pUuid = new ParcelUuid(UUID.fromString(BLE_UUID));
        mBleAdvertiseData = new AdvertiseData.Builder()
                .setIncludeDeviceName( true )
                .setIncludeTxPowerLevel(true)
                .addServiceUuid( pUuid )
                //.addServiceData( pUuid, "Data".getBytes("utf-8" ) )
                .build();
    }

    private void discover(){
    }

    AdvertiseCallback mBleAdvCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            device_status.setText("Advertising");
            device_data.setText(mBleAdvertiseData.toString());
            device_settings.setText(mAdvertiserSettings.toString());
            super.onStartSuccess(settingsInEffect);
        }

        @Override
        public void onStartFailure(int errorCode) {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.append(errorCode);
            device_status.setText("Error: " + errorMsg);
            device_data.setText(mBleAdvertiseData.toString());
            device_settings.setText(mAdvertiserSettings.toString());
            super.onStartFailure(errorCode);
        }
    };
}
