package com.example.ble_application02;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertisingSet;
import android.bluetooth.le.AdvertisingSetCallback;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.BluetoothLeAdvertiser;

//Class is used when BLE 5 is supported
public class BLE_5_system {
    private BluetoothLeAdvertiser mBle5_advertiser;
    private  AdvertisingSetParameters parameters;
    private AdvertiseData data;
    private MainActivity ma;

    public BLE_5_system(BluetoothAdapter adapter, MainActivity main){
        mBle5_advertiser = adapter.getBluetoothLeAdvertiser();
        ma = main;
    }

    public void ble5Advertise(){
        parameters = (new AdvertisingSetParameters.Builder())
                .setLegacyMode(true)
                .setConnectable(false)
                .setInterval(AdvertisingSetParameters.INTERVAL_HIGH)
                .setTxPowerLevel(AdvertisingSetParameters.TX_POWER_HIGH)
                .build();
        data = (new AdvertiseData.Builder())
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(true)
                .build();
    }

    public void ble5start(){
        mBle5_advertiser.startAdvertisingSet(parameters, data, null, null, null, mBle5Callback);
    }

    public void ble5stop(){
        mBle5_advertiser.stopAdvertisingSet(mBle5Callback);
    }
    AdvertisingSetCallback mBle5Callback = new AdvertisingSetCallback() {
        @Override
        public void onAdvertisingSetStarted(AdvertisingSet advertisingSet, int txPower, int status) {
            ma.device_status.setText("Advertising");
            ma.device_data.setText(data.toString());
            ma.device_settings.setText(parameters.toString());
            super.onAdvertisingSetStarted(advertisingSet, txPower, status);
        }

        @Override
        public void onAdvertisingSetStopped(AdvertisingSet advertisingSet) {
            super.onAdvertisingSetStopped(advertisingSet);
        }

        @Override
        public void onAdvertisingDataSet(AdvertisingSet advertisingSet, int status) {
            super.onAdvertisingDataSet(advertisingSet, status);
        }

        @Override
        public void onScanResponseDataSet(AdvertisingSet advertisingSet, int status) {
            super.onScanResponseDataSet(advertisingSet, status);
        }

        @Override
        public void onAdvertisingParametersUpdated(AdvertisingSet advertisingSet, int txPower, int status) {
            super.onAdvertisingParametersUpdated(advertisingSet, txPower, status);
        }
    };
}

